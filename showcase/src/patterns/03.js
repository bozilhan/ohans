import React, {
    createContext,
    useCallback,
    useContext,
    useEffect,
    useLayoutEffect,
    useMemo,
    useRef,
    useState
} from 'react';
import mojs from 'mo-js';
import styles from './index.css';


const useClapAnimation = ({clapEl, countEl, clapTotalEl}) => {
    const [animationTimeline, setAnimationTimeline] = useState(
    () => new mojs.Timeline()
    );
    
    useLayoutEffect(() => {
        if (!clapEl || !countEl || !clapTotalEl) {
            return;
        }
        
        const tlDuration = 300;
        const scaleButton = new mojs.Html({
            el: clapEl,
            duration: tlDuration,
            scale: {1.3: 1},
            easing: mojs.easing.ease.out
        });
        
        const triangleBurst = new mojs.Burst({
            parent: clapEl,
            radius: {50: 95},
            count: 5,
            angle: 30,
            children: {
                shape: 'polygon',
                radius: {6: 0},
                stroke: 'rgba(211,54,0,0.5)',
                strokeWidth: 2,
                angle: 210,
                delay: 30,
                speed: 0.2,
                easing: mojs.easing.bezier(0.1, 1, 0.3, 1),
                duration: tlDuration
            }
        });
        
        const circleBurst = new mojs.Burst({
            parent: clapEl,
            radius: {50: 75},
            angle: 30,
            duration: tlDuration,
            children: {
                shape: 'circle',
                fill: 'rgba(149,165,166,0.5)',
                delay: 30,
                speed: 0.2,
                radius: {3: 0},
                easing: mojs.easing.bezier(0.1, 1, 0.3, 1)
            }
        });
        
        const countAnimation = new mojs.Html({
            el: countEl,
            opacity: {0: 1},
            // go up 30 pixels
            y: {0: -30},
            duration: tlDuration
        }).then({
            opacity: {1: 0},
            // 80 pixels up
            y: -80,
            delay: tlDuration >> 1
        });
        
        const countTotalAnimation = new mojs.Html({
            el: clapTotalEl,
            opacity: {0: 1},
            delay: 3 * (tlDuration >> 1),
            duration: tlDuration,
            y: {0: -3}
        });
        
        if (typeof clapEl === 'string') {
            const clap = document.getElementById('clap');
            clap.style.transform = 'scale(1,1)';
        } else {
            clapEl.style.transform = 'scale(1,1)';
        }
        
        const newAnimationTimeline = animationTimeline.add([
            scaleButton,
            countTotalAnimation,
            countAnimation,
            triangleBurst,
            circleBurst
        ]);
        
        setAnimationTimeline(newAnimationTimeline);
    }, [clapEl, countEl, clapTotalEl]);
    
    return animationTimeline;
};

/*
 * SECTION4
 * COMPOUND COMPONENTS
 *
 * WHY?
 * 1. Customizability
 * expose SPECIFIC PROPS for EACH OF CHILD and
 * make specific props very CUSTOMIZABLE IN THEIR OWN SPACE
 *
 * props of ClapIcon DONT HAVE TO BE THE SAME for CountTotal
 * If I want to do something SPECIFIC you have only CUSTOMIZED CountTotal SPECIFICALLY
 * then I just go ahead and open them up for THEIR OWN INDIVIDUAL PROPS
 *
 *
 * 2. Understandable
 * we expose a VERY UNDERSTANDABLE COMPONENT
 * the developer understands easily what is going on there
 *
 *
 * 3. Props Overload
 * if we can avoid props overload if the prop doesnt have to go into PARENT COMPONENT
 * then bu exposing the CHILD component.
 * we can JUST SEND in the RIGHT PROP to the RIGHT COMPONENT
 *
 *
 * HOW TO IMPLEMENT THE COMPOUND COMPONENTS PATTERN
 * we are going to EXPOSE(ortaya cikarmak) the SMALLER COMPOENENTS AS CHILDREN COMPONENTS to the PARENT
 * PARENT component CONTROLS the MOVEMENT of the CHILD components.
 * Actual motion STARTS of in PARENT component and ALL the OTHER CHILDREN RECEIVE the INSTRUCTION and they move along
 * ALL THING HAPPENS in PARENT and EVERY OTHER CHILD LISTENS and RECEIVES the INSTRUCTION and moves along
 *
 * in our own case the MediumClap is going to ACT LIKE A LOCOMOTIVE.
 * All of the logic, the IMPORTANT UI STATE LOGIC is going to HAPPEN within MediumClap(PARENT)
 * and EVERY CHILD is going to ACT LIKE A CARRIAGES.
 * EVERY CHILD RECEIVE THE INSTRUCTIONS FORM PARENT
 * so EVERYTHING they NEED is going to be PASSED in to them
 * BUT THE ACTUAL LOGIC IS GOING TO BE SIT IN ONE PLACE within the PARENT(LOCOMOTIVE) component
 *
 * <select>
 *    <option id='1'>val1</option>
 *    <option id='2'>val2</option>
 * </select>
 * ayni durum select ve option html elementler arasinda da soz konusu
 * option works hand-in-hand with select and moves accordingly
 *
 * we create a CONTEXT OBJECT in PARENT within the CHILD COMPONENTS we will hook them up
 * the context object and child components can RECEIVE WHATEVER PROPS THEY NEED FROM the CONTEXT OBJECT
 *
 */
const initialState = {
    count: 0,
    countTotal: 267,
    isClicked: false
};

/*
  STEP1
  provide a context object for which it's going
  to HOLD ALL THE STATE VALUES and
  ALL THE VALUES REQUIRED BY THE CHILD COMPONENTS

  to create context object use createContext() method

  context is designed to SHARE DATA that can be considered 'GLOBAL'
  for a tree of react components
  so all the data that we need to SHARE AMONG all the CHILD components SHOULD SIT IN A CONTEXT OBJECT in PARENT(MediumClap)
  Then we get data off the context object within the child components
*/
const MediumClapContext = createContext();

/*
  STEP2
  context object has a Provider
  deconstruct provider

  provider is a component that PROVIDES the values NEEDED by
  ALL THE CHILD COMPONENTS

  provider is going to wrap all of components
  for which you want to access the data in the context object
*/
const {Provider} = MediumClapContext;

/*
  STEP4
  children i parent componente prop olarak gec
  JSX de de child componentleri {children} olarak tanimla

  onClap callback prop INTERNAL STATEIN DISARIDAN ERISILMESI ICIN EKLENDI!!!
  STEP4 ile ilgisi yok.
  Internal statei DISARI ACMAYACAKSAK onClap callback e GEREK YOK!!!
  MediumClapi(PARENT) baska developer/client/user kullanacaksa
  count, countTotal, isClicked kendi componentlerinde gerekliyse onClap callback ZORUNLU!!!
*/
const MediumClap = ({children, onClap}) => {
    const MAXIMUM_USER_CLAP = 50;
    const [clapState, setClapState] = useState(initialState);
    
    const {count} = clapState;
    
    const [{clapRef, clapCountRef, clapTotalRef}, setRefsState] = useState({});
    
    const setRef = useCallback(node => {
        setRefsState(prevRefsState => ({
            ...prevRefsState,
            [node.dataset.refkey]: node
        }));
    }, []);
    
    const animationTimeline = useClapAnimation({
        clapEl: clapRef,
        countEl: clapCountRef,
        clapTotalEl: clapTotalRef
    });
    
    const componentJustMounted = useRef(true);
    
    /*
    every single time count changes or user clicks we want to invoke this callback
  
    useeffect will be called ON MOUNT and and dependency array(bu ornekte count) changes
    */
    useEffect(() => {
        if (!componentJustMounted.current) {
            console.log('onClap was called');
            onClap && onClap(clapState);
        }
        
        componentJustMounted.current = false;
    }, [count]);
    
    const handleClapClick = () => {
        animationTimeline.replay();
        
        setClapState(prevState => ({
            isClicked: true,
            count: Math.min(prevState.count + 1, MAXIMUM_USER_CLAP),
            countTotal:
            count < MAXIMUM_USER_CLAP
            ? prevState.countTotal + 1
            : prevState.countTotal
        }));
    };
    
    /*
     STEP5
     Provider value propertysine child componentlerin ihtiyaci olan proplari
     memoized olarak ekle
     we want this re computed if state changes and if setRef changes
  
     But this is not a very good idea.
     because any time the MediumClap is re-rendered based off of a change in some outer component
     the provider will be re-rendered because we're passing a new object in here.
     And if the provider component is re-rendered the old tree of components get re-rendered as well.
             <Provider value={{...clapState, clapEl, countEl, clapTotalEl}}>
    
     therefore we use useMemo() hook
     
     All this values READY to be CONSUMED by any CHILD component in the tree
*/
    const memoizedValue = useMemo(
    () => ({
        ...clapState,
        setRef
    }),
    [clapState, setRef]
    );
    
    /*
        STEP3
        wrap all child components to <Provider>
        Provider component takes a REQUIRED value prop
        this is then VALUE you want to make available to the ENTIRE TREE OF COMPONENTS
        in our case the value is 'setRef' and all the 'clapState'
      */
    return (
    <Provider value={memoizedValue}>
        <button
        ref={setRef}
        data-refkey='clapRef'
        className={styles.clap}
        onClick={handleClapClick}
        >
            {children}
        </button>
    </Provider>
    );
};

const ClapIcon = () => {
    /*
      STEP6
      because we want to child components to GET THESE VALUES OFF(inmek,ayrilmak,cikmak) of the PROVIDER
  
      to get a value off a provider or context object use useContext() hook
      useContext() RETRIEVES VALUE SAVED FROM THE PROVIDER
  
      Componente prop olarak gecmiyoruz degerini ICERIDE SET EDIYORUZ
      */
    const {isClicked} = useContext(MediumClapContext);
    
    return (
    <span>
      <svg
      xmlns='http://www.w3.org/2000/svg'
      viewBox='0 0 297.221 297.221'
      className={`${styles.icon} ${isClicked && styles.checked}`}
      >
        <path
        d='M283.762 32.835a6 6 0 001.432-8.363 5.998 5.998 0 00-8.363-1.432l-14.984 10.602a6 6 0 106.931 9.794l14.984-10.601zM244.064 29.387a5.973 5.973 0 002.11.386 6 6 0 005.617-3.891l6.46-17.182a6 6 0 10-11.233-4.222l-6.46 17.182a6 6 0 003.506 7.727zM291.223 55.611h-.124l-18.351.154c-3.313.067-5.944 2.605-5.877 5.918.066 3.271 2.739 5.928 5.997 5.928h.124l18.351-.313c3.313-.068 5.944-2.732 5.877-6.045-.066-3.271-2.739-5.642-5.997-5.642zM254.2 147.154c-3.073-2.433-6.711-4.089-10.557-4.867.254-.46.491-.928.715-1.403l2.408-2.408c9.274-9.275 10.248-23.874 2.264-33.961a25.235 25.235 0 00-14.812-9.106c.415-.764.783-1.545 1.117-2.338 6.316-9.149 6.213-21.445-.782-30.283a25.236 25.236 0 00-14.818-9.117c4.8-8.826 4.187-19.826-2.225-27.925-4.848-6.125-12.109-9.639-19.923-9.639-6.257 0-12.16 2.236-16.792 6.33a24.913 24.913 0 00-5.012-11.169c-4.849-6.125-12.11-9.638-19.924-9.639-6.79 0-13.164 2.635-17.947 7.418l-60.84 60.84-.232-8.12c-.107-13.83-11.392-25.049-25.247-25.049-13.604 0-24.729 10.815-25.229 24.298l-12.146 88.306-9.983 11.604c-5.983 6.957-5.582 17.481.915 23.962L19.987 199.7c-4.574 6.881-3.773 16.266 2.206 22.23l69.667 69.557a17.507 17.507 0 0012.446 5.148c3.857 0 7.668-1.295 10.729-3.645l14.544-11.168c13.991-3.305 29.416-10.813 45.874-22.33 14.371-10.058 29.962-23.46 46.337-39.836l34.631-34.631a25.408 25.408 0 007.375-19.427 25.425 25.425 0 00-9.596-18.444zM188.124 32.009a13.342 13.342 0 019.462-3.903c3.915 0 7.831 1.695 10.515 5.086 4.256 5.377 3.51 13.18-1.339 18.028l-6.177 6.176c-.952.635-1.879 1.314-2.747 2.083a24.906 24.906 0 00-5.013-11.169 25.272 25.272 0 00-12.475-8.527l7.774-7.774zm-41.727-14.477c2.602-2.602 6.032-3.903 9.462-3.903 3.916.001 7.831 1.696 10.515 5.087 4.256 5.377 3.51 13.179-1.339 18.027l-70.919 70.186-.233-8.119c-.061-7.825-3.7-14.812-9.356-19.405l61.87-61.873zM13.624 176.391a5.618 5.618 0 01-.291-7.64l12.281-14.277a.052.052 0 00.012-.026l12.72-92.483c0-7.286 5.961-13.247 13.247-13.247s13.248 5.961 13.248 13.247L65.186 74c-11.988 1.646-21.322 11.733-21.78 24.057l-12.145 88.307-3.533 4.108-14.104-14.081zm234.311.148l-34.63 34.631c-29.577 29.577-60.494 53.318-87.653 59.237a5.723 5.723 0 00-2.271 1.043l-15.655 12.022a5.6 5.6 0 01-3.419 1.162 5.607 5.607 0 01-3.968-1.641l-69.671-69.56a5.616 5.616 0 01-.291-7.64l12.28-14.276a.058.058 0 00.013-.026l12.719-92.483c0-7.286 5.962-13.248 13.248-13.248s13.247 5.962 13.247 13.248l.626 21.824c.104 3.626 3.087 5.987 6.191 5.987 1.514 0 3.058-.563 4.309-1.813l70.431-70.431c2.603-2.603 6.031-3.903 9.462-3.903 3.915 0 7.831 1.695 10.515 5.086 4.256 5.377 3.509 13.18-1.34 18.028l-48.518 48.518a6.449 6.449 0 000 9.121c1.275 1.275 2.946 1.913 4.617 1.913s3.343-.638 4.617-1.913l62.374-62.373c2.602-2.603 6.031-3.903 9.462-3.903 3.915.001 7.831 1.696 10.515 5.087 4.256 5.376 3.509 13.179-1.34 18.027l-62.081 62.081a6.538 6.538 0 000 9.246 6.421 6.421 0 004.556 1.887 6.422 6.422 0 004.555-1.887l48.811-48.81c2.603-2.603 6.032-3.903 9.462-3.903 3.915 0 7.831 1.695 10.515 5.087 4.256 5.376 3.509 13.179-1.34 18.027l-48.349 48.35a6.687 6.687 0 000 9.458l.078.079a6.161 6.161 0 004.37 1.81 6.158 6.158 0 004.37-1.81l29.974-29.974a14.08 14.08 0 019.921-4.129c2.867 0 5.726.904 8.107 2.789 6.36 5.034 6.754 14.403 1.181 19.975z'/>
      </svg>
    </span>
    );
};

const ClapCount = () => {
    /*
      STEP6
      because we want to child components to GET THESE VALUES OFF(inmek,ayrilmak,cikmak) of the PROVIDER
  
      to get a value off a provider or context object use useContext() hook
      useContext() RETRIEVES VALUE SAVED FROM THE PROVIDER
  
      Componente prop olarak gecmiyoruz degerini ICERIDE SET EDIYORUZ
      */
    const {count, setRef} = useContext(MediumClapContext);
    return (
    <span ref={setRef} data-refkey='clapCountRef' className={styles.count}>
      {count}
    </span>
    );
};

const CountTotal = () => {
    /*
      STEP6
      because we want to child components to GET THESE VALUES OFF(inmek,ayrilmak,cikmak) of the PROVIDER
  
      to get a value off a provider or context object use useContext() hook
      useContext() RETRIEVES VALUE SAVED FROM THE PROVIDER
  
      Componente prop olarak gecmiyoruz degerini ICERIDE SET EDIYORUZ
      */
    const {countTotal, setRef} = useContext(MediumClapContext);
    return (
    <span ref={setRef} data-refkey='clapTotalRef' className={styles.total}>
      {countTotal}
    </span>
    );
};

/*
Hepsi ayni dosya icerisinde olmali
import MediumClap, {ClapIcon, ClapCount, CountTotal} from 'aaa'
const Usage = () => {
    return (
    <MediumClap>
        <ClapIcon/>
        <ClapCount/>
        <CountTotal/>
    </MediumClap>
    );
};


ALTERNATIVE USAGE
for convenience, but totally not required...

MediumClap.Icon is reference to ClapIcon
MediumClap.Count is reference to ClapCount
MediumClap.Total is reference to CountTotal
referenslar child component tanimlarindan sonra gerceklestirilmeli
*/
MediumClap.Icon = ClapIcon;
MediumClap.Count = ClapCount;
MediumClap.Total = CountTotal;

/*
  in the Usage we NEVER PASS as CHILD PROPS
  because we want to child components to GET THESE VALUES OFF(inmek,ayrilmak,cikmak) of the PROVIDER
*/
const Usage = () => {
    /*
    what if the OUTSIDE world the client/developer/user of our component wanted to do something
    any of count, countTotal, isClicked
  
    even though we have count, countTotal, isClicked state properties with a MediumClap
    there are in NO WAY COMMUNICATED to the OUTSIDE WORLD
  
    make the compound component pattern even more EXTENSIBLE we HAVE TO PROVIDE CALLBACK
    every single time someone clicks/claps we invoke the onClap CALLBACK but we pass it ALL THE clapState
    been managed INTERNALLY
    so that developer/client/user can take ALL STATE and do whatever they want to do
  
  
    Disariya acmak istedigimiz statei burada belirliyoruz. Ornegin 'count'
    Parent componente callback propu ekliyoruz
    Parent componentte ilgili callback propu mountta calismayacak sekilde useeffect icerisine koyup
    parent componentteki internal statele state degisimini sagliyoruz
  
    handleClap metoduna TUM STATEI PARAMETRE OLARAK GECTIK.
    Icerisinde DISARIYLA PAYLASILMAK ISTENEN PROPU BELIRLEDIK
    */
    const [count, setCount] = useState(0);
    const handleClap = clapState => {
        setCount(clapState.count);
    };
    
    /*
    import MediumClap, {ClapIcon,ClapCount,CountTotal} from 'medium-clap'
      ALTERNATIVE
      return (
        <MediumClap>
          <ClapIcon />
          <ClapCount />
          <CountTotal />
        </MediumClap>
      );
  
      in the usage we are NOT passing PROPS
      useContext ile her child componentin ihtiyaci olan/ilgilendigi propu provider dan aliyoruz
  
  
      import MediumClap from 'medium-clap'
      developer does not have to import all the child components
      they just import CONTAINER/PARENT component
    */
    return (
    <div style={{width: '100%'}}>
        <MediumClap onClap={handleClap}>
            <MediumClap.Icon/>
            <MediumClap.Count/>
            <MediumClap.Total/>
        </MediumClap>
        {!!count && (
        <div className={styles.info}>{`You have clapped ${count} times`}</div>
        )}
    </div>
    );
};

export default Usage;
