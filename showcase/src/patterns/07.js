import React, {
    useCallback,
    useEffect,
    useLayoutEffect,
    useRef,
    useState
} from 'react';
import mojs from 'mo-js';
import styles from './index.css';


const useClapAnimation = ({clapEl, countEl, clapTotalEl}) => {
    const [animationTimeline, setAnimationTimeline] = useState(
    () => new mojs.Timeline()
    );
    
    useLayoutEffect(() => {
        if (!clapEl || !countEl || !clapTotalEl) {
            return;
        }
        
        const tlDuration = 300;
        const scaleButton = new mojs.Html({
            el: clapEl,
            duration: tlDuration,
            scale: {1.3: 1},
            easing: mojs.easing.ease.out,
        });
        
        const triangleBurst = new mojs.Burst({
            parent: clapEl,
            radius: {50: 95},
            count: 5,
            angle: 30,
            children: {
                shape: 'polygon',
                radius: {6: 0},
                stroke: 'rgba(211,54,0,0.5)',
                strokeWidth: 2,
                angle: 210,
                delay: 30,
                speed: 0.2,
                easing: mojs.easing.bezier(0.1, 1, 0.3, 1),
                duration: tlDuration,
            },
        });
        
        const circleBurst = new mojs.Burst({
            parent: clapEl,
            radius: {50: 75},
            angle: 30,
            duration: tlDuration,
            children: {
                shape: 'circle',
                fill: 'rgba(149,165,166,0.5)',
                delay: 30,
                speed: 0.2,
                radius: {3: 0},
                easing: mojs.easing.bezier(0.1, 1, 0.3, 1),
            },
        });
        
        const countAnimation = new mojs.Html({
            el: countEl,
            opacity: {0: 1},
            y: {0: -30},
            duration: tlDuration,
        }).then({
            opacity: {1: 0},
            y: -80,
            delay: tlDuration >> 1,
        });
        
        const countTotalAnimation = new mojs.Html({
            el: clapTotalEl,
            opacity: {0: 1},
            delay: 3 * (tlDuration >> 1),
            duration: tlDuration,
            y: {0: -3},
        });
        
        if (typeof clapEl === 'string') {
            const clap = document.getElementById('clap');
            clap.style.transform = 'scale(1,1)';
        } else {
            clapEl.style.transform = 'scale(1,1)';
        }
        
        const newAnimationTimeline = animationTimeline.add([
            scaleButton,
            countTotalAnimation,
            countAnimation,
            triangleBurst,
            circleBurst,
        ]);
        
        setAnimationTimeline(newAnimationTimeline);
    }, [clapEl, countEl, clapTotalEl]);
    
    return animationTimeline;
};

/*
* SECTION8
* PROPS COLLECTION
*
* Compound componentle ilgisi yok genel olarak kullanilacak bir durum
*
* Train componentin ihtiyaci olan seat strap foo-> some other prop
* <Train seat strap foo/>
* <Train seat strap foo/>
* <Train seat strap foo/>
*
* Train componenti kullanan herkes ilgili proplari her seferinde yazmak ZORUNDA
* multiple multiple multiple times
*
* if we could provide COLLECTION that HOLDS this 3 values(seat strap foo)
* so the user dont have to write these 3 props. User would just say collection
* for example just passing the COLLECTION that we've PROVIDED INTERNALLY
* collection u biz SAGLIYORUZ
* We will do all the DIRTY work for user and user just USES the PROP COLLECTION
* <Train collection/>
* <Train collection/>
* <Train collection/>
*
* That is exactly what the PROPS COLLECTION PATTERN is a collection is just an OBJECT or ARRAY
*
* collection is just an OBJECT OF PROP VALUES for which WE KNOW the USER will NEED
*
* all we know the based off(temel almak) of the usage of our component
* this collection is important for the USER. he/she needs this collection
* so WE(COMPONENTI TASARLAYAN, COMPONENTIN IHTIYACI OLAN PROPLARI BILEN, BEN)
* PROVIDE(saglamak, temin etmek, ihtiyacini karsilamak) it
*
* collection nesnesinin icerisine ilgili componentin DUZGUN CALISMASI, ISE YARAMAZ OLMAMASI icin
* gereken OLMAZSA OLMAZ proplari koyuyoruz. THE MOST IMPORTANT PROPS passed on
* optional proplar(sona ekledigim =[], ={}, =0 vs gibi olanlar) EKLENMIYOR!!!
* HEAVILY HEAVILY dependent on the kind of component you are building
*
* Sagladigimiz prop collection nesnesi baska componentlerde de kullanilabilir
* <div {...myPropCollection}>{ONE_OF_THE_KEYS_OF_PROP_COLLECTION_OBJECT}</div>
* <ClapCount {...myPropCollection}>{ONE_OF_THE_KEYS_OF_PROP_COLLECTION_OBJECT}</div>
*
* <ClapCount setRef={setRef} data-refkey='clapCountRef' {..counterProps}/>
* <div {...counterProps}>{count}</div>
* div kullandigimizda ClapCount animasyonu calismaz fakat her tiklandiginda counter artar
*
* providing props collection is not JUST perform a convenience. It also MAKES EASIER for the USER
* TO REPLACE my internal UI components and still have the MOST IMPORTANT props passed on to
* whatever other want to use
*
*
* SECTION9
* PROPS GETTERS
* collection nesnelerine atanan degerler benim gelistirdigim componenti kullananlar tarafindan DEGISTIRILEMEZ
* Bu durumun ustesinden gelmek icin props getters kullanilir
*
* props getters kullanilmazsa da kullanici oncelikle collection propsu spread ile acar
* daha sonra da ilgili keylerden birine istedigini atayabilir
* <div {...collectionProps} onClick={hisOwnClick}/>
*
* props collection is an OBJECT
* props getters is a FUNCTION
*/
const INITIAL_STATE = {
    count: 0,
    countTotal: 267,
    isClicked: false,
};

const useDOMRef = () => {
    const [DOMRef, setRefsState] = useState({});
    
    const setRef = useCallback((node) => {
        setRefsState((prevRefsState) => ({
            ...prevRefsState,
    
            [node.dataset.refkey]: node,
        }));
    }, []);
    
    return [DOMRef, setRef];
};

/*
* STEP9.8
* kendi internal metodumuzdan sonra kullanicinin tanimladigi metodun cagrilmasini gerceklestirmek ici
* yazilan method
*
* takes a number of FUNCTIONS and it call all those functions
* in SEQUENCE, IN THE SAME ORDER they have passed
*
* rest operator ilgili metodun cagrildigi yerdeki tum parametrelerini toplar
* rest parameter represents an INDEFINITE NUMBERS OF ARGUMENTS as an ARRAY
* ... converts list of function arguments into an array
* and then we can iterate that array (...fns)
*
* mevcut ornekte dugmeye bastigimizda count un arttiktan sonra loga CLICKED!!! yazildigini goruruz.
* Bu metodu yazmasaydik sadece logda CLICKED!!! gorurduk counter artmazdi (OVERRIDE)
*
* onClick requires a function expects a function to be passed of the UI element
* to be used as a CALLBACK to be invoked when user clicks
* onClick e fonksiyon atamamiz gerekir * default olarak * onClick: () => {}
* dolayisiyla
*
* callFnsInSequence takes arbitrary number of FUNCTIONS and we RETURN ANOTHER FUNCTION
* returned function is what is in fact passed into onClick
*
*
* curried function
* https://stackoverflow.com/questions/37763828/javascript-es6-double-arrow-functions/37764035
* https://stackoverflow.com/questions/32782922/what-do-multiple-arrow-functions-mean-in-javascript/32784025
* */
const callFnsInSequence = (...fns) => (...args) => {
    /*
    * if fn exists we go ahead and invoke the function by passing all the arguments
    * */
    fns.forEach(fn => fn && fn(...args));
};

const useClapState = (initialState = INITIAL_STATE) => {
    const MAXIMUM_USER_CLAP = 50;
    const [clapState, setClapState] = useState(initialState);
    const {count, countTotal} = clapState;
    
    const updateClapState = useCallback(() => {
        setClapState(({count, countTotal}) => ({
            isClicked: true,
            count: Math.min(count + 1, MAXIMUM_USER_CLAP),
            countTotal:
            count < MAXIMUM_USER_CLAP
            ? countTotal + 1
            : countTotal,
        }));
    }, [count, countTotal]);
    
    /*
    * SECTION8
    * provide ACCESSIBILITY values that aid accessibility
    * toggler is the content of the button
    * const togglerProps = {
    *   onClick: updateClapState,
    *   'aria-pressed': clapState.isClicked
    * }
    *
    * SECTION9
    * STEP9.3
    * nesneleri metoda cevir RETURNS the SAME PROPS COLLECTION
    *
    * STEP9.5
    * rest --> combine
    * rest operator is specified in the FUNCTION DECLARATION
    *
    * STEP9.7
    * Kullanicinin internal metodu ezmesi yerine internal metoddan sonra kendi metodunu cagirmasi icin
    * ilk olarak onClick i deconstruct ediyoruz
    * daha sonra callFnsInSequence ile oncelikle kendi internal metodumuzun ardindan kullacinin tanimladigi
    * metodun cagrilmasini sagliyoruz
    * */
    const getTogglerProps = ({onClick, ...otherProps} = {}) => ({
        onClick: callFnsInSequence(updateClapState, onClick),
        'aria-pressed': clapState.isClicked,
        ...otherProps
    });
    
    /*
    * SECTION8
    * provide ACCESSIBILITY values that aid accessibility
    * every single user who uses the counterProps collection is going to
    * have 'count' for free and all of this accessibility gains ('aria-XXX') for free
    * const counterProps = {
    *   count,
        'aria-valuemax': MAXIMUM_USER_CLAP,
        'aria-valuemin': 0,
        'aria-valuenow': count,
    * }
    *
    * SECTION9
    * STEP9.3
    * nesneleri metoda cevir RETURNS the SAME PROPS COLLECTION
    *   const getCounterProps = () => ({
            count,
            'aria-valuemax': MAXIMUM_USER_CLAP,
            'aria-valuemin': 0,
            'aria-valuenow': count
        });
    * STEP9.1, 9.2, 9.3 ve 9.4 tamamlandiktan sonra SECTION8 deki duruma gelinir. Problemsiz calisir
    *
    * STEP9.6
    * rest --> combine
    * rest operator is specified in the FUNCTION DECLARATION
    * ... metod tanimlanirken kullanildiginda rest operator
    * ... metod cagrildiginda kullanildiginda spread operator. 2. kullanim spread
    */
    const getCounterProps = ({...otherProps}) => ({
        count,
        'aria-valuemax': MAXIMUM_USER_CLAP,
        'aria-valuemin': 0,
        'aria-valuenow': count,
        ...otherProps
    });
    
    /*
    * SECTION9
    * STEP9.4
    * custom hookdan metodlari don. PARANTEZ YOK. REFERANS, function call DEGIL!!!
    */
    return {clapState, updateClapState, getTogglerProps, getCounterProps};
};

const useEffectAfterMount = (cb, deps) => {
    const componentJustMounted = useRef(true);
    useEffect(() => {
        if (!componentJustMounted.current) {
            return cb();
        }
        componentJustMounted.current = false;
    }, deps);
};

const ClapContainer = ({children, setRef, handleClick, ...restProps}) => {
    return (
    <button
    ref={setRef}
    className={styles.clap}
    onClick={handleClick}
    {...restProps}
    >
        {children}
    </button>
    );
};
/*
* SECTION 8
* ClapIcon yerine emoji kullandik
* Kod hala olmasi gibi calisiyor
* */
const ClapIcon = ({isClicked}) => {
    return (
    <span>
      <svg
      xmlns='http://www.w3.org/2000/svg'
      viewBox='0 0 297.221 297.221'
      className={`${styles.icon} ${isClicked && styles.checked}`}
      >
        <path
        d='M283.762 32.835a6 6 0 001.432-8.363 5.998 5.998 0 00-8.363-1.432l-14.984 10.602a6 6 0 106.931 9.794l14.984-10.601zM244.064 29.387a5.973 5.973 0 002.11.386 6 6 0 005.617-3.891l6.46-17.182a6 6 0 10-11.233-4.222l-6.46 17.182a6 6 0 003.506 7.727zM291.223 55.611h-.124l-18.351.154c-3.313.067-5.944 2.605-5.877 5.918.066 3.271 2.739 5.928 5.997 5.928h.124l18.351-.313c3.313-.068 5.944-2.732 5.877-6.045-.066-3.271-2.739-5.642-5.997-5.642zM254.2 147.154c-3.073-2.433-6.711-4.089-10.557-4.867.254-.46.491-.928.715-1.403l2.408-2.408c9.274-9.275 10.248-23.874 2.264-33.961a25.235 25.235 0 00-14.812-9.106c.415-.764.783-1.545 1.117-2.338 6.316-9.149 6.213-21.445-.782-30.283a25.236 25.236 0 00-14.818-9.117c4.8-8.826 4.187-19.826-2.225-27.925-4.848-6.125-12.109-9.639-19.923-9.639-6.257 0-12.16 2.236-16.792 6.33a24.913 24.913 0 00-5.012-11.169c-4.849-6.125-12.11-9.638-19.924-9.639-6.79 0-13.164 2.635-17.947 7.418l-60.84 60.84-.232-8.12c-.107-13.83-11.392-25.049-25.247-25.049-13.604 0-24.729 10.815-25.229 24.298l-12.146 88.306-9.983 11.604c-5.983 6.957-5.582 17.481.915 23.962L19.987 199.7c-4.574 6.881-3.773 16.266 2.206 22.23l69.667 69.557a17.507 17.507 0 0012.446 5.148c3.857 0 7.668-1.295 10.729-3.645l14.544-11.168c13.991-3.305 29.416-10.813 45.874-22.33 14.371-10.058 29.962-23.46 46.337-39.836l34.631-34.631a25.408 25.408 0 007.375-19.427 25.425 25.425 0 00-9.596-18.444zM188.124 32.009a13.342 13.342 0 019.462-3.903c3.915 0 7.831 1.695 10.515 5.086 4.256 5.377 3.51 13.18-1.339 18.028l-6.177 6.176c-.952.635-1.879 1.314-2.747 2.083a24.906 24.906 0 00-5.013-11.169 25.272 25.272 0 00-12.475-8.527l7.774-7.774zm-41.727-14.477c2.602-2.602 6.032-3.903 9.462-3.903 3.916.001 7.831 1.696 10.515 5.087 4.256 5.377 3.51 13.179-1.339 18.027l-70.919 70.186-.233-8.119c-.061-7.825-3.7-14.812-9.356-19.405l61.87-61.873zM13.624 176.391a5.618 5.618 0 01-.291-7.64l12.281-14.277a.052.052 0 00.012-.026l12.72-92.483c0-7.286 5.961-13.247 13.247-13.247s13.248 5.961 13.248 13.247L65.186 74c-11.988 1.646-21.322 11.733-21.78 24.057l-12.145 88.307-3.533 4.108-14.104-14.081zm234.311.148l-34.63 34.631c-29.577 29.577-60.494 53.318-87.653 59.237a5.723 5.723 0 00-2.271 1.043l-15.655 12.022a5.6 5.6 0 01-3.419 1.162 5.607 5.607 0 01-3.968-1.641l-69.671-69.56a5.616 5.616 0 01-.291-7.64l12.28-14.276a.058.058 0 00.013-.026l12.719-92.483c0-7.286 5.962-13.248 13.248-13.248s13.247 5.962 13.247 13.248l.626 21.824c.104 3.626 3.087 5.987 6.191 5.987 1.514 0 3.058-.563 4.309-1.813l70.431-70.431c2.603-2.603 6.031-3.903 9.462-3.903 3.915 0 7.831 1.695 10.515 5.086 4.256 5.377 3.509 13.18-1.34 18.028l-48.518 48.518a6.449 6.449 0 000 9.121c1.275 1.275 2.946 1.913 4.617 1.913s3.343-.638 4.617-1.913l62.374-62.373c2.602-2.603 6.031-3.903 9.462-3.903 3.915.001 7.831 1.696 10.515 5.087 4.256 5.376 3.509 13.179-1.34 18.027l-62.081 62.081a6.538 6.538 0 000 9.246 6.421 6.421 0 004.556 1.887 6.422 6.422 0 004.555-1.887l48.811-48.81c2.603-2.603 6.032-3.903 9.462-3.903 3.915 0 7.831 1.695 10.515 5.087 4.256 5.376 3.509 13.179-1.34 18.027l-48.349 48.35a6.687 6.687 0 000 9.458l.078.079a6.161 6.161 0 004.37 1.81 6.158 6.158 0 004.37-1.81l29.974-29.974a14.08 14.08 0 019.921-4.129c2.867 0 5.726.904 8.107 2.789 6.36 5.034 6.754 14.403 1.181 19.975z'/>
      </svg>
    </span>
    );
};

const ClapCount = ({count, setRef, ...restProps}) => {
    return (
    <span
    ref={setRef}
    className={styles.count}
    {...restProps}
    >
      {count}
    </span>
    );
};

const CountTotal = ({countTotal, setRef, ...restProps}) => {
    return (
    <span
    ref={setRef}
    className={styles.total}
    {...restProps}
    >
      {countTotal}
    </span>
    );
};

const Usage = () => {
    /*
    * SECTION8 Kullanim
    * STEP8.1
    * statei idame ettirdigimiz custom hook a eklenir
    * props collection for 'count'
    * when the PROPERTY name is SAME AS the VARIABLE name
    * we can reduce variable name EQUIVALENT
    * const counterProps={
    *   count
    * };
    * EQUIVALENT
    * const counterProps={
    *   count:count
    * };
    *
    * props collection for 'click'
    const togglerProps={
        onClick:updateClapState
    };
    * Returns object
    * return {clapState, updateClapState, togglerProps, counterProps};
    *
    * STEP8.2
    * custom hooku kullandigimiz yerde karsila
    * const {clapState, updateClapState, togglerProps, counterProps} = useClapState();
    *
    * STEP8.3
    * Ilgili componente spread operatorle gec
    * <ClapContainer
        setRef={setRef}
        data-refkey='clapRef'
        {...tooglerProps}>
        
     <ClapCount setRef={setRef}
        {..counterProps}
        data-refkey='clapCountRef'/>
    
    * Sagladigimiz prop collection nesnesi baska componentlerde de kullanilabilir
    * <div {...myPropCollection}>{ONE_OF_THE_KEYS_OF_PROP_COLLECTION_OBJECT}</div>
    * <ClapCount {...myPropCollection}>{ONE_OF_THE_KEYS_OF_PROP_COLLECTION_OBJECT}</div>
    *
    * <ClapCount setRef={setRef} data-refkey='clapCountRef' {..counterProps}/>
    * <div {...counterProps}>{count}</div>
    * div kullandigimizda ClapCount animasyonu calismaz fakat her tiklandiginda counter artar
    *
    *
    * SECTION9
    * STEP9.1
    * const {clapState, updateClapState, togglerProps, counterProps} = useClapState();
    * const {clapState, updateClapState, getTogglerProps, getCounterProps} = useClapState();
    * olarak degistirilir
    *
    * STEP9.2
    * <ClapCount setRef={setRef}
           {...counterProps}
           data-refkey='clapCountRef'/>
      <ClapCount setRef={setRef}
         {...getCounterProps()}
         data-refkey='clapCountRef'/>
    * Nesne spreadlerini function calla cevir. SONDAKI PARANTEZE DIKKAT
    * referans DEGIL FUNCTION CALL
    */
    const {clapState, updateClapState, getTogglerProps, getCounterProps} = useClapState();
    const {count, countTotal, isClicked} = clapState;
    
    const [{clapRef, clapCountRef, clapTotalRef}, setRef] = useDOMRef();
    
    const animationTimeline = useClapAnimation({
        clapEl: clapRef,
        countEl: clapCountRef,
        clapTotalEl: clapTotalRef,
    });
    
    useEffectAfterMount(() => {
        animationTimeline.replay();
        
    }, [count]);
    
    const handleClick = () => {
        console.log('CLICKED!!');
    };
    
    /*
    * SECTION8
    * collection nesnesinin icerisine ilgili componentin DUZGUN CALISMASI, ISE YARAMAZ OLMAMASI icin
    * gereken OLMAZSA OLMAZ proplari koyuyoruz.
    * optional proplar(sona ekledigim =[], ={}, =0 vs gibi olanlar) EKLENMIYOR!!!
    * HEAVILY HEAVILY dependent on the kind of component you are building
    *
    * Every user who uses ClapContainer has to pass onClick.
    * If he/she doesnt pass onClick prop the ClapContainer is USELESS!!!
    * ClapContainer doesnt do ANYTHING
    * onCLick prop MUTLAKA collection nesnesine eklenmeli
    *
    * if I dont pass the count prop ClapCount is useless. It does NOTHING!!!
    * count collection nesnesine eklenmeli
    *
    * animasyon componentlerimizin calismasi icin ZORUNLULUK GEREKTIRMEDIGINDEN setRef ve data-refKey
    * collection nesnesine EKLENMEDI.
    * kullanicilara benim tarafimdan sadece count ve onClick benim tarafimdan collection object icerisinde saglanmali
    * cunku ilgili 2 prop componentlerin ise yaramasi icin ZORUNLU!!!
    *
    *
    * as soon as we pass onClick here it OVERRIDES INTERNAL onClick
    * our animation is broken!!!
    *
    *
    * CountTotal a gecilen data-refkey='clapTotalRef' komponentin kendisinde ...restProps ile karsilaniyor
    *
    * STEP9.5
    * getter metodlara object gecilir {onClick: handleClick, 'aria-pressed': false }
    * buradaki onClick ...otherProps un icerisinde getTogglerProps() a gecilir
    * ... metod tanimlanirken kullanildiginda rest operator
    * ... metod cagrildiginda kullanildiginda spread operator
    * using the SPREAD operator array gets split into the INDIVIDUAL ELEMENTS
    * and those individual elements are again COMBINED into a SINGLE ARRAY
    * onClick e buradan deger atamak internal statede yer alan metodu ezer(override)
    */
    return (
    <ClapContainer
    setRef={setRef}
    data-refkey='clapRef'
    {...getTogglerProps({
        onClick: handleClick,
        'aria-pressed': false
    })}
    >
        🤩
        <ClapCount setRef={setRef}
                   {...getCounterProps()}
                   data-refkey='clapCountRef'/>
        <CountTotal setRef={setRef} countTotal={countTotal}
                    data-refkey='clapTotalRef'/>
    </ClapContainer>
    );
};

export default Usage;
