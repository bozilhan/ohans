import React, {Component, useState} from 'react';
/*
 * animation mo-js
 */
import mojs from 'mo-js';
import styles from './index.css';

/*
 * HOC RECAP
 * HOC sets off(belirginlestirmek) the foundation(temel) of everything
 *
 * to get animations work in first off we are going to do this
 * by IMPLEMENTING the higher order components(HOC)
 *
 * HOC is a technique for REUSING COMPONENT LOGIC we add
 * 
 * why are we using HOC
 * 
 *
 * HOC is just a FUNCTION that TAKES a COMPONENT and RETURNS ANOTHER COMPONENT
 * after HOC transformation the COMPONENT gains ADDITIONAL FUNCTIONALITY --> Custom Hook 
 * 
 * HOC starts with 'with' by convention with 
 */
const withClapAnimation = WrappedComponent => {
  /*
   * we want to return a NEW COMPONENT that has the SAME FUNCTIONALITY
   * as WHATEVER was passed in which SOME ADDITIONAL LAYER ADDED ON TOP (PURPLE STRIPE)
   */
  class WithClapAnimation extends Component {
    //handles animation logic
    // animate = () => {
    //     console.log('%c Animate', 'background:yellow;color:black');
    // };
    animationTimeline = new mojs.Timeline();

    state = {
      animationTimeline: this.animationTimeline
    };

    // buttona tiklandiginda buttonun scalei degisiyor
    // tiklandiktan sonra 300 ms suruyor
    componentDidMount() {
      const tlDuration = 300;
      const scaleButton = new mojs.Html({
        // target element
        // id of clap
        el: '#clap',
        duration: tlDuration,
        // {startScale:endScale}
        scale: {1.3: 1},
        // to have slow ending --> ease.out
        // not linear
        easing: mojs.easing.ease.out
      });

      const countAnimation = new mojs.Html({
        // target element
        // id of clapCount
        el: '#clapCount',
        opacity: {0: 1},
        // go up 30 pixels
        y: {0: -30},
        duration: tlDuration
      }).then({
        // then
        // USTEKILERIN HEPSI TAMAMLANDIKTAN SONRA
        // BURAYA DEVAM ET VE BURADAKILERI DE GERCEKLESTIR.

        // go from 1 up onto 0
        // GET HIDDEN AGAIN
        opacity: {1: 0},
        // 80 pixels up
        y: -80,
        delay: tlDuration >> 1
      });

      // Kirmizi ucgenlere burst adi verilir
      // kirmizi ucgenler svg formatinda oldugundan
      // tum svg ozelliklerini burada kulanabiliriz.
      const triangleBurst = new mojs.Burst({
        parent: '#clap',
        // how far does this burst come out from
        // from 50 to 95 pixels
        radius: {50: 95},
        // particle count
        count: 5,
        angle: 30,
        children: {
          // individual particles
          // default of polygon is triangle
          shape: 'polygon',
          radius: {6: 0},
          stroke: 'rgba(211,54,0,0.5)',
          strokeWidth: 2,
          angle: 210,
          //ms
          delay: 30,
          speed: 0.2,
          easing: mojs.easing.bezier(0.1, 1, 0.3, 1),
          duration: tlDuration
        }
      });

      const circleBurst = new mojs.Burst({
        parent: '#clap',
        radius: {50: 75},
        angle: 30,
        duration: tlDuration,
        children: {
          // individual particles
          shape: 'circle',
          fill: 'rgba(149,165,166,0.5)',
          delay: 30,
          speed: 0.2,
          // triangle is bigger two times than circle
          radius: {3: 0},
          easing: mojs.easing.bezier(0.1, 1, 0.3, 1)
        }
      });

      const countTotalAnimation = new mojs.Html({
        // target element
        // id of clapCountTotal
        el: '#clapCountTotal',
        opacity: {0: 1},
        delay: 3 * (tlDuration >> 1),
        duration: tlDuration,
        // orijinden -3 yukari ziplama
        // tiklandiginda kaybolur
        // daha sonra 3 pixel yukarida gorunur
        y: {0: -3}
      });

      const clap = document.getElementById('clap');
      clap.style.transform = 'scale(1,1)';

      // AnimationTimeLine a eklemeyi UNUTMA!!!
      const newAnimationTimeline = this.animationTimeline.add([
        scaleButton,
        countTotalAnimation,
        countAnimation,
        triangleBurst,
        circleBurst
      ]);

      this.setState({
        animationTimeline: newAnimationTimeline
      });
    }

    // class component HAS TO render() method
    render() {
      return (
        <WrappedComponent
          {...this.props}
          animationTimeline={this.state.animationTimeline}
          // animate={this.animate}
        />
      );
    }
  }

  return WithClapAnimation;
};

/*
 * button component altindakiler child elementler oluyor
 */
const initialState = {
  count: 0,
  countTotal: 267,
  isClicked: false
};

const MediumClap = ({animationTimeline}) => {
  // const MediumClap = ({animate}) => {
  const MAXIMUM_USER_CLAP = 12;
  const [clapState, setClapState] = useState(initialState);

  /*
    destructure the clapState
    parent componentin stateti FARKLI CHILDLARA serpistirilecekse!!!
    count ClapCount da isClicked ClapIcon da countTotal CountTotal de kullaniliyor

    eger ilgili 3 state tek bir componente gecseydi parenttan {...clapState} ile gonderip
    child component de de ({count, countTotal, isClicked}) olarak karsilardik
    */
  const {count, countTotal, isClicked} = clapState;

  const handleClapClick = () => {
    animationTimeline.replay();
    // animate();
    /*
     * as soon as user clicks
     * 1. update count --> count + 1
     * 2. update countTotal --> countTotal + 1
     *
     * MORE CONSISTENT WAY of getting the PREVIOUS STATE and UPDATING THAN
     *
     * arrow functiondan OBJECT donrururken
     * ({key:vale}) ya da {return {key:value}}
     *
     * setState hangi tipte veri set ediyorsa BIR ONCEKI statei alip
     * ILGILI TIPTEN YENI VERI URETIYORUZ
     */
    setClapState(prevState => ({
      isClicked: true,
      count: Math.min(prevState.count + 1, MAXIMUM_USER_CLAP),
      countTotal:
        count < MAXIMUM_USER_CLAP ? prevState.countTotal + 1 : prevState.countTotal
    }));
  };

  return (
    <button id='clap' className={styles.clap} onClick={handleClapClick}>
      <ClapIcon isClicked={isClicked} />
      <ClapCount count={count} />
      <CountTotal countTotal={countTotal} />
    </button>
  );
};

/*
 * subcomponents
 *
 * className={`${styles.icon} ${isClicked && styles.checked}`}
 * by default styles.icon
 * but if isClicked PASS ANOTHER STYLE styles.checked
 */
const ClapIcon = ({isClicked}) => {
  return (
    <span>
      <svg
        xmlns='http://www.w3.org/2000/svg'
        viewBox='0 0 297.221 297.221'
        className={`${styles.icon} ${isClicked && styles.checked}`}
      >
        <path d='M283.762 32.835a6 6 0 001.432-8.363 5.998 5.998 0 00-8.363-1.432l-14.984 10.602a6 6 0 106.931 9.794l14.984-10.601zM244.064 29.387a5.973 5.973 0 002.11.386 6 6 0 005.617-3.891l6.46-17.182a6 6 0 10-11.233-4.222l-6.46 17.182a6 6 0 003.506 7.727zM291.223 55.611h-.124l-18.351.154c-3.313.067-5.944 2.605-5.877 5.918.066 3.271 2.739 5.928 5.997 5.928h.124l18.351-.313c3.313-.068 5.944-2.732 5.877-6.045-.066-3.271-2.739-5.642-5.997-5.642zM254.2 147.154c-3.073-2.433-6.711-4.089-10.557-4.867.254-.46.491-.928.715-1.403l2.408-2.408c9.274-9.275 10.248-23.874 2.264-33.961a25.235 25.235 0 00-14.812-9.106c.415-.764.783-1.545 1.117-2.338 6.316-9.149 6.213-21.445-.782-30.283a25.236 25.236 0 00-14.818-9.117c4.8-8.826 4.187-19.826-2.225-27.925-4.848-6.125-12.109-9.639-19.923-9.639-6.257 0-12.16 2.236-16.792 6.33a24.913 24.913 0 00-5.012-11.169c-4.849-6.125-12.11-9.638-19.924-9.639-6.79 0-13.164 2.635-17.947 7.418l-60.84 60.84-.232-8.12c-.107-13.83-11.392-25.049-25.247-25.049-13.604 0-24.729 10.815-25.229 24.298l-12.146 88.306-9.983 11.604c-5.983 6.957-5.582 17.481.915 23.962L19.987 199.7c-4.574 6.881-3.773 16.266 2.206 22.23l69.667 69.557a17.507 17.507 0 0012.446 5.148c3.857 0 7.668-1.295 10.729-3.645l14.544-11.168c13.991-3.305 29.416-10.813 45.874-22.33 14.371-10.058 29.962-23.46 46.337-39.836l34.631-34.631a25.408 25.408 0 007.375-19.427 25.425 25.425 0 00-9.596-18.444zM188.124 32.009a13.342 13.342 0 019.462-3.903c3.915 0 7.831 1.695 10.515 5.086 4.256 5.377 3.51 13.18-1.339 18.028l-6.177 6.176c-.952.635-1.879 1.314-2.747 2.083a24.906 24.906 0 00-5.013-11.169 25.272 25.272 0 00-12.475-8.527l7.774-7.774zm-41.727-14.477c2.602-2.602 6.032-3.903 9.462-3.903 3.916.001 7.831 1.696 10.515 5.087 4.256 5.377 3.51 13.179-1.339 18.027l-70.919 70.186-.233-8.119c-.061-7.825-3.7-14.812-9.356-19.405l61.87-61.873zM13.624 176.391a5.618 5.618 0 01-.291-7.64l12.281-14.277a.052.052 0 00.012-.026l12.72-92.483c0-7.286 5.961-13.247 13.247-13.247s13.248 5.961 13.248 13.247L65.186 74c-11.988 1.646-21.322 11.733-21.78 24.057l-12.145 88.307-3.533 4.108-14.104-14.081zm234.311.148l-34.63 34.631c-29.577 29.577-60.494 53.318-87.653 59.237a5.723 5.723 0 00-2.271 1.043l-15.655 12.022a5.6 5.6 0 01-3.419 1.162 5.607 5.607 0 01-3.968-1.641l-69.671-69.56a5.616 5.616 0 01-.291-7.64l12.28-14.276a.058.058 0 00.013-.026l12.719-92.483c0-7.286 5.962-13.248 13.248-13.248s13.247 5.962 13.247 13.248l.626 21.824c.104 3.626 3.087 5.987 6.191 5.987 1.514 0 3.058-.563 4.309-1.813l70.431-70.431c2.603-2.603 6.031-3.903 9.462-3.903 3.915 0 7.831 1.695 10.515 5.086 4.256 5.377 3.509 13.18-1.34 18.028l-48.518 48.518a6.449 6.449 0 000 9.121c1.275 1.275 2.946 1.913 4.617 1.913s3.343-.638 4.617-1.913l62.374-62.373c2.602-2.603 6.031-3.903 9.462-3.903 3.915.001 7.831 1.696 10.515 5.087 4.256 5.376 3.509 13.179-1.34 18.027l-62.081 62.081a6.538 6.538 0 000 9.246 6.421 6.421 0 004.556 1.887 6.422 6.422 0 004.555-1.887l48.811-48.81c2.603-2.603 6.032-3.903 9.462-3.903 3.915 0 7.831 1.695 10.515 5.087 4.256 5.376 3.509 13.179-1.34 18.027l-48.349 48.35a6.687 6.687 0 000 9.458l.078.079a6.161 6.161 0 004.37 1.81 6.158 6.158 0 004.37-1.81l29.974-29.974a14.08 14.08 0 019.921-4.129c2.867 0 5.726.904 8.107 2.789 6.36 5.034 6.754 14.403 1.181 19.975z' />
      </svg>
    </span>
  );
};

const ClapCount = ({count}) => {
  return (
    <span id='clapCount' className={styles.count}>
      {count}
    </span>
  );
};

const CountTotal = ({countTotal}) => {
  return (
    <span id='clapCountTotal' className={styles.total}>
      {countTotal}
    </span>
  );
};

/*
 * USAGE of HOC
 * MediumClap has no animation logic
 * Due to passing MediumClap to HOC MediumClap HAS animation logic
 */
const Usage = () => {
  const AnimatedMediumClap = withClapAnimation(MediumClap);
  return <AnimatedMediumClap />;
};

export default Usage;
